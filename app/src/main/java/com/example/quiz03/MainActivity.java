package com.example.quiz03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.HashMap;

class button1 implements inter{
    @Override
    public void temp(TextView v){
        v.setText("button1");
    }
}

class button2 implements inter{
    @Override
    public void temp(TextView v){
        v.setText("button2");
    }
}

public class MainActivity extends AppCompatActivity {
    HashMap <Integer, inter> btn = new HashMap<>();
    TextView text;
    Button btn1,btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.textView);
        btn1 = findViewById(R.id.button1);
        btn2 = findViewById(R.id.button2);
        btn.put(R.id.button1,new button1());
        btn.put(R.id.button2,new button2());

        Button.OnClickListener onClickListener = new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.d("asdf",v.getId() + "");
                btn.get(v.getId()).temp(text);
            }
        };

        btn1.setOnClickListener(onClickListener);
        btn2.setOnClickListener(onClickListener);

    }

}
